package com.razzzil.attestation3.controller;

import com.razzzil.attestation3.model.WebUser;
import com.razzzil.attestation3.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
@RequestMapping("/")
public class AuthController {

    private final AuthService authService;

    @GetMapping
    public String index(){
        return "index";
    }

    @PostMapping("/signUp")
    public String register(@RequestParam("name") String name,
                         @RequestParam("login") String login,
                         @RequestParam("password") String password,
                         Model model){
        WebUser webUser = authService.register(name, login, password);
        model.addAttribute("user", webUser);
        return "index";
    }

}
