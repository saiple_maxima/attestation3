package com.razzzil.attestation3.repository;

import com.razzzil.attestation3.model.WebUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WebUserRepository extends JpaRepository<WebUser, Long> {
}
