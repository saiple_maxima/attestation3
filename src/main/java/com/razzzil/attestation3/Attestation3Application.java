package com.razzzil.attestation3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Attestation3Application {

    public static void main(String[] args) {
        SpringApplication.run(Attestation3Application.class, args);
    }

}
