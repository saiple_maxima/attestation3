package com.razzzil.attestation3.service;

import com.razzzil.attestation3.model.WebUser;
import com.razzzil.attestation3.repository.WebUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final WebUserRepository webUserRepository;

    public WebUser register(String name, String login, String password){
        WebUser webUser = WebUser.builder()
                .passwordHash(password)
                .login(login)
                .name(name)
                .build();
        return webUserRepository.save(webUser);
    }
}
